using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using msxml3net;
using WSS3WebServices.ListsWS;
using System.Diagnostics;

namespace WSScom
{

    public partial class WSSWS
    {
        private Lists listsService = new Lists();
        private bool bInitlistService = false;

        

        /// <summary>
        /// internal
        /// </summary>
        private void InitlistsService()
        {
            if (!bInitsCredential)
            {
                listsService.Credentials = CredentialCache.DefaultCredentials;
            }
            else
            {
                listsService.Credentials = sCredential;
            }
            if (bInitsUrl)
            {
                listsService.Url = sUrl + "_vti_bin/lists.asmx";
            }
            if (bInitsUseragent)
            {
                listsService.UserAgent = sUseragent;
            }
            bInitlistService = true;
        }

        /// <summary>
        /// Add an list based on the specified name, description, and list template ID.
        /// </summary>
        /// <param name="listName">A string that contains the title of the list.</param>
        /// <param name="description">A string that contains a description for the list.</param>
        /// <param name="templateID">A 32-bit integer that specifies the list template to use. The following table shows possible values for the ID.
        /// <value><list type="table">
        /// <listheader>
        /// <term>Type</term><description>List Display Name</description>
        /// </listheader>
        /// <item><term>104</term><description>Announcements</description></item>
        /// <item><term>105</term><description>Contacts</description></item>
        /// <item><term>100</term><description>Custom List</description></item>
        /// <item><term>120</term><description>Custom List in Datasheet View</description></item>
        /// <item><term>110</term><description>DataSources</description></item>
        /// <item><term>108</term><description>Discussion Board</description></item>
        /// <item><term>101</term><description>Document Library</description></item>
        /// <item><term>106</term><description>Events</description></item>
        /// <item><term>115</term><description>Form Library</description></item>
        /// <item><term>1100</term><description>Issues</description></item>
        /// <item><term>103</term><description>Links</description></item>
        /// <item><term>109</term><description>Picture Library</description></item>
        /// <item><term>102</term><description>Survey</description></item>
        /// <item><term>106</term><description>Tasks</description></item>
        /// </list>
        /// </value>
        /// </param>
        /// <returns>A fragment in Collaborative Application Markup Language (CAML) contains information about the new list in DOMDocument object.</returns>
        /// <example>Create list
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domreturn = New DOMDocument();
        /// domreturn = oWS.AddList("Shared Documents", "new documents", 101);
        /// </code>
        /// </example>
        public DOMDocument AddList(string listName, string description, int templateID)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            XmlNode lResponse = null;

            try
            {
                lResponse = listsService.AddList(listName, description, templateID);
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            gXMLResponse = lResponse;
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Allows documents to be checked in to a SharePoint document library remotely. 
        /// </summary>
        /// <param name="fileRef">A string that contains URL path to the document to check in.</param>
        /// <param name="comment">A string containing optional check-in comments</param>
        /// <param name="CheckinType">A string representation of the values 0, 1 or 2, where 0 = MinorCheckIn, 1 = MajorCheckIn, and 2 = OverwriteCheckIn.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <returns>A fragment in Collaborative Application Markup Language (CAML) contains information about the new list in DOMDocument object.</returns>
        /// <example>Check in document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.CheckInFile("sites/site/Shared Documents/test.doc", "minor update", "0");
        /// </code>
        /// </example>
        public bool CheckInFile(string fileRef, string comment, string CheckinType)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            bool lResponse = false;

            try
            {
                lResponse = listsService.CheckInFile(sUrl + fileRef, comment, CheckinType);
                if (!lResponse)
                {
                    gXMLResponse = Handler.WSSWSException("<errorstring>unknown error</errorstring>");
                }

            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;
        }

        /// <summary>
        /// Allows documents in a SharePoint document library to be checked out remotely. 
        /// </summary>
        /// <param name="fileRef">A string that contains the URL path to the document to be checked out.</param>
        /// <param name="checkoutToLocal">A string containing "true" or "false" that designates whether the file is to be flagged as checked out for offline editing.</param>
        /// <param name="lastmodified">A string in RFC 1123 date format representing the date and time of the last modification to the file; for example, "20 Jun 1982 12:00:00 GMT" or <c>null</c>.</param>
        /// <!--If this parameter contains a value, the server compares the submitted lastModified value with the stored lastModified value. If the values do not match, the check-out fails and this method returns false.-->
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Check out document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.CheckOutFile("sites/site/Shared Documents/test.doc", "true", "20 Jun 1982 12:00:00 GMT");
        /// </code>
        /// </example>
        public bool CheckOutFile(string fileRef, string checkoutToLocal, string lastmodified)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            bool lResponse = false;

            try
            {
                lResponse = listsService.CheckOutFile(sUrl + fileRef, checkoutToLocal, lastmodified);
                if (!lResponse)
                {
                    gXMLResponse = Handler.WSSWSException("<errorstring>unknown error</errorstring>");
                }

            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;
        }

        /// <summary>
        /// Undoes the check-out of a given document in a SharePoint Document Library. 
        /// </summary>
        /// <param name="fileRef">A string that contains URL path to the document.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Undo check out document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.CheckOutFile("sites/site/Shared Documents/test.doc");
        /// </code>
        /// </example>
        public bool UndoCheckOut(string fileRef)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            bool lResponse = false;

            try
            {
                lResponse = listsService.UndoCheckOut(sUrl + fileRef);
                if (!lResponse)
                {
                    gXMLResponse = Handler.WSSWSException("<errorstring></errorstring>");
                }
            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;
        }

        /// <summary>
        /// Returns the names and GUIDs for all lists in the site. 
        /// </summary>
        /// <returns>Names and GUIDs for all lists in the site in DOMDocument object.</returns>
        /// <example>Get list collection
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domreturn = new DOMDocument();
        /// domreturn = oWS.GetListCollection();
        /// </code>
        /// </example>
        public DOMDocument GetListCollection()
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            XmlNode lResponse = null;

            try
            {
                lResponse = listsService.GetListCollection();
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            gXMLResponse = lResponse;
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Delete the specified list.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Delete list
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.DeleteList("Shared Documents");
        /// </code>
        /// </example>
        public bool DeleteList(string listName)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            bool lResponse = false;

            try
            {
                listsService.DeleteList(listName);
                lResponse = true;
            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;

        }

        /// <summary>
        /// internal method
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="viewName"></param>
        /// <param name="query"></param>
        /// <param name="viewFields"></param>
        /// <param name="rowLimit"></param>
        /// <param name="queryOptions"></param>
        /// <param name="webID"></param>
        /// <returns></returns>
        private XmlNode priGetListItems(string listName, string viewName, XmlNode query, XmlNode viewFields, string rowLimit, XmlNode queryOptions, string webID)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            XmlNode lResponse = null;

            try
            {
                lResponse = listsService.GetListItems(listName, viewName, query, viewFields, rowLimit, queryOptions, webID);
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;
        }

        /// <summary>
        /// internal
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="viewName"></param>
        /// <param name="query"></param>
        /// <param name="viewFields"></param>
        /// <param name="rowLimit"></param>
        /// <param name="queryOptions"></param>
        /// <param name="webID"></param>
        /// <returns></returns>
        private DOMDocument GetListItems(
            string listName, 
            string viewName,
            DOMDocument query,
            DOMDocument viewFields, 
            string rowLimit, 
            DOMDocument queryOptions, 
            string webID
            )
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            XmlNode lResponse = null;

            try
            {
                lResponse = listsService.GetListItems(
                    listName, 
                    viewName, 
                    Handler.DOMDocument2XmlNode(query), 
                    Handler.DOMDocument2XmlNode(viewFields), 
                    rowLimit, 
                    Handler.DOMDocument2XmlNode(queryOptions), 
                    webID
                    );
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// TODO:AddListFields
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="newFields"></param>
        /// <returns></returns>
        private DOMDocument AddListFields(string listName, DOMDocument newFields)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse;

            try
            {
                lResponse = priUpdateList(
                    listName,
                    null,
                    Handler.DOMDocument2XmlNode(newFields),
                    null,
                    null,
                    null
                    );
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// TODO:UpdateList
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="listProperties"></param>
        /// <param name="newFields"></param>
        /// <param name="updateFields"></param>
        /// <param name="deleteFields"></param>
        /// <param name="listVersion"></param>
        /// <returns></returns>
        private DOMDocument UpdateList(
            string listName,
            DOMDocument listProperties,
            DOMDocument newFields,
            DOMDocument updateFields,
            DOMDocument deleteFields,
            string listVersion
            )
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse;

            try
            {
                lResponse = priUpdateList(
                    listName,
                    Handler.DOMDocument2XmlNode(listProperties),
                    Handler.DOMDocument2XmlNode(newFields),
                    Handler.DOMDocument2XmlNode(updateFields),
                    Handler.DOMDocument2XmlNode(deleteFields),
                    listVersion
                    );
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return Handler.XmlNode2DOMDocument(lResponse);

        }

        /// <summary>
        /// internal
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="listProperties"></param>
        /// <param name="newFields"></param>
        /// <param name="updateFields"></param>
        /// <param name="deleteFields"></param>
        /// <param name="listVersion"></param>
        /// <returns></returns>
        private XmlNode priUpdateList(
            string listName,
            XmlNode listProperties,
            XmlNode newFields,
            XmlNode updateFields,
            XmlNode deleteFields,
            string listVersion
            )
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse;

            try
            {
                lResponse = listsService.UpdateList(
                    listName,
                    listProperties,
                    newFields,
                    updateFields,
                    deleteFields,
                    listVersion
                    );
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return lResponse;

        }

        /// <summary>
        /// TODO:UpdateListItems
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="updates"></param>
        /// <returns></returns>
        private DOMDocument UpdateListItems(
            string listName,
            DOMDocument updates
            )
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse;

            try
            {
                lResponse = listsService.UpdateListItems(
                    listName,
                    Handler.DOMDocument2XmlNode(updates)
                    );
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }

            return Handler.XmlNode2DOMDocument(lResponse);

        }

        /// <summary>
        /// internal
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="updates"></param>
        /// <returns></returns>
        private XmlNode priUpdateListItems(string listName, XmlNode updates)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            
            XmlNode lResponse;

            try
            {
                lResponse = listsService.UpdateListItems(listName, updates);
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }

        /// <summary>
        /// Adds an attachment to the specified list item in the specified list. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="listItemID">A string that contains the ID of the item to which attachments are added. This value does not correspond to the index of the item within the collection of list items.</param>
        /// <param name="filePath">A string that contains local file path <c>c:\folder\file.doc</c></param>
        /// <returns>A string that contains the URL path for the attachment, which can subsequently be used to reference the attachment.</returns>
        /// <example>Add attachment
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// string strreturn = null;
        /// string idtask = null;
        /// idtask = oWSS.GetDocumentID("Tasks", "new task")
        /// if (idtask != null)
        /// {
        ///     strreturn = oWS.AddAttachment("Shared Documents", idtask, "c:\test.doc");
        /// }
        /// </code>
        /// </example>
        public string AddAttachment(string listName, string listItemID, string filePath)
        {

            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            string lResponse = null;

            //[Serialization.XmlElementAttribute(DataType="base64Binary")] byte[] 
            if (!File.Exists(filePath))
            {
                throw new ArgumentException(String.Format("{0} does not exist", filePath), "fileName");
            }

            FileStream fStream = File.OpenRead(filePath);
            string fileName = fStream.Name.Substring(3);
            byte[] attachment = new byte[fStream.Length];
            fStream.Read(attachment, 0, (int)fStream.Length);
            fStream.Close();

            try
            {
                lResponse = listsService.AddAttachment(listName, listItemID, fileName, attachment);
            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }




        /// <summary>
        /// Delete the attachment from the specified list item.
        /// </summary>
        /// <param name="listName">A string that contains either the title or GUID for the list.</param>
        /// <param name="listItemID">A string that contains the ID of the item to delete. This value does not correspond to the index of the item within the collection of list items.</param>
        /// <param name="url">A string that contains the absolute URL for the attachment, as follows: http://Server_Name/Site_Name/Lists/List_Name/Attachments/Item_ID/FileName.doc</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Delete attachment
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// string strreturn = null;
        /// string idtask = null;
        /// idtask = oWSS.GetDocumentID("Tasks", "new task")
        /// if (idtask != null)
        /// {
        ///     strreturn = oWS.DeleteAttachment("Tasks", idtask, "http://Server_Name/Site_Name/Lists/Tasks/Attachments/" + idtask + "/test.doc");
        /// }
        /// </code>
        /// </example>
        public bool DeleteAttachment(string listName, string listItemID, string url)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            bool lResponse = false;
            try
            {
                listsService.DeleteAttachment(listName, listItemID, url);
                lResponse = true;
            }
            catch (SoapException ex)
            {
                gXMLResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }

        /// <summary>
        /// Returns a list of the URLs for attachments to the specified item. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="listItemID">A string that contains the ID for the list item. This value does not correspond to the index of the item within the collection of list items.</param>
        /// <returns>An XML fragment in the following form that contains the URLs for the attachments and that can be assigned to a DOMDocument object. 
        /// <example>
        /// <code>
        /// <Attachments>
        ///    <Attachment>http://Server_Name/LISTS/List_Name/Attachments/Item_ID/File1_Name</Attachment>
        ///    <Attachment>http://Server_Name/LISTS/List_Name/Attachments/Item_ID/File2_Name</Attachment>
        /// </Attachments>
        /// </code>
        /// </example>
        /// </returns>
        /// <example>Get attachment collection
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// string strreturn = null;
        /// string idtask = null;
        /// idtask = oWSS.GetDocumentID("Tasks", "new task")
        /// if (idtask != null)
        /// {
        ///     strreturn = oWS.GetAttachmentCollection("Tasks", idtask);
        /// }
        /// </code>
        /// </example>

        public DOMDocument GetAttachmentCollection(string listName, string listItemID)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse = null;
            try
            {
                lResponse = listsService.GetAttachmentCollection(listName, listItemID);
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Returns a document collection of specified list and folder. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="folderName">A string that contains either the folder name or the <c>null</c> for the root.</param>
        /// <example>Get document collection
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domreturn = new DOMDocument();
        /// domreturn = oWS.GetDocumentCollection("Shared Documents", "Shared Documents/subfolder");
        /// </code>
        /// </example>
        /// <returns>Document attributes assigned to DOMDocument.</returns>
        public DOMDocument GetDocumentCollection(string listName, string folderName)
        {

            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }

            XmlDocument xmldoc = new XmlDocument();
            XmlNode query = xmldoc.CreateNode(XmlNodeType.Element, "Query", "");
            XmlNode queryOptions = xmldoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            if (folderName != "")
            {
                queryOptions.InnerXml = "<Folder>" + folderName + "</Folder>";
            }
            else
            {
            }

            XmlNode lResponse = null;
            XmlNode lGetList = null;
            string rowLimit = null;

            try
            {
                lGetList = listsService.GetList(listName);
                rowLimit = lGetList.Attributes["ItemCount"].Value;// .SelectSingleNode("/parameter[@name='ItemCount']").InnerText;
            }
            catch (Exception ex)
            {

            }

            try
            {
                lResponse = priGetListItems(listName, null, query, null, rowLimit, queryOptions, null);
            }
            catch
            {
            }
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Returns a schema for the specified list. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list. When querying the UserInfo table, the string contains "UserInfo".</param>
        /// <returns>A fragment in Collaborative Application Markup Language (CAML) in the following form that contains the list schema and that can be assigned to a DOMDocument object.</returns>
        /// <example>Get list
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domreturn = new DOMDocument();
        /// domreturn = oWS.GetDocumentCollection("Shared Documents");
        /// </code>
        /// </example>
        public DOMDocument GetList(string listName)
        {
            //initialize listsService
            if (!bInitlistService)
            {
                InitlistsService();
            }
            XmlNode lResponse = null;
            try
            {
                lResponse = listsService.GetList(listName);
            }
            catch (SoapException ex)
            {
                lResponse = Handler.SoapExceptions(ex);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Get the document and save to filePath.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="fileRef">A string that contains the URL path in current website for the document.</param>
        /// <param name="filePath">A string that contains local file path <c>c:\folder\file.doc</c></param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Get document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.GetDocument("Shared Documents", "Shared Documents/test.doc", "c:\test.doc");
        /// </code>
        /// </example>
        public bool GetDocument(string listName, string fileRef, string filePath)
        {
            //initialize WebClient
            WebClient webClient = new WebClient();
            //initialize WebClient Credentials
            if (!bInitsCredential)
            {
                webClient.Credentials = CredentialCache.DefaultCredentials;
            }
            else
            {
                webClient.Credentials = sCredential;
            }


            bool lResponse = false;

            try
            {
                webClient.DownloadFile(sUrl + fileRef, filePath);
                lResponse = true;
            }
            catch (WebException ex)
            {
                gXMLResponse = Handler.WebExceptions(ex);
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            return lResponse;
        }

        /// <summary>
        /// Returns ID of document. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <returns>ID of document if the operation succeeded; otherwise, <c>null</c></returns>
        public string GetDocumentID(string listName, string fileRef)
        {
            string temp;
            temp = "<Query><Where><Eq><FieldRef Name='FileRef' /><Value Type='Text'>" + fileRef + "</Value></Eq></Where></Query>";

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(temp);
            XmlNode query = xmldoc;

            XmlNode queryOptions = xmldoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            string folderName = null;

            folderName = getFolderFromfileRef(listName, fileRef);

            if (folderName != null)
            {
                queryOptions.InnerXml = "<Folder>" + folderName + "</Folder>";
            }
            else
            {
            }

            string id = null;

            try
            {
                XmlNode caml = priGetListItems(listName, null, query, null, "1", queryOptions, null);
                if (caml != null)
                {
                    if (caml.HasChildNodes)
                    {
                        if (caml.ChildNodes[1].HasChildNodes)
                        {
                            XmlElement attr = caml.ChildNodes[1].ChildNodes[1] as XmlElement;
                            if (attr != null)
                            {
                                if (attr.HasAttribute("ows_ID"))
                                {
                                    id = caml.ChildNodes[1].ChildNodes[1].Attributes["ows_ID"].Value;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return id;
        }

        /// <summary>
        /// Return document checkout state. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <returns><c>true</c> if document checked out; otherwise, <c>false</c>.</returns>
        /// <example>Get document checkout state
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.GetDocumentCheckState("Shared Documents", "sites/site/Shared Documents/test.doc");
        /// </code>
        /// </example>
        public bool GetDocumentCheckState(string listName, string fileRef)
        {
            string temp;
            temp = "<Query><Where><Eq><FieldRef Name='FileRef' /><Value Type='Text'>" + fileRef + "</Value></Eq></Where></Query>";

            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(temp);
            XmlNode query = xmldoc;

            XmlNode queryOptions = xmldoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

            //if (folderName != "")
            //{
            //    queryOptions.InnerXml = "<Folder>" + folderName + "</Folder>";
            //}
            //else
            //{
            //}


            bool checkout = false;

            try
            {
                XmlNode caml = priGetListItems(listName, null, query, null, "1", queryOptions, null);
                if (caml != null)
                {
                    if (caml.HasChildNodes)
                    {
                        if (caml.ChildNodes[1].HasChildNodes)
                        {
                            XmlElement attr = caml.ChildNodes[1].ChildNodes[1] as XmlElement;
                            if (attr != null )
                            {
                                if (attr.HasAttribute("ows_CheckoutUser"))
                                {
                                    checkout = true;
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
            }
            return checkout;
        }

        /// <summary>
        /// Delete the document from the specified list.
        /// </summary>
        /// <param name="listName">A string that contains either the title or GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Delete document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.DeleteDocument("Shared Documents", "sites/site/Shared Documents/test.doc");
        /// </code>
        /// </example>
        public bool DeleteDocument(string listName, string fileRef)
        {
            bool lResponse = false;
            try
            {
                string ID = GetDocumentID(listName, fileRef);
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='Delete'>" +
                        "<Field Name='ID'>" + ID + "</Field>" +
                        "<Field Name='FileRef'>" + fileRef + "</Field>" +
                        "</Method>" +
                        "</Batch>";


                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(
                        listName,
                        updates
                        );
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }

        /// <summary>
        /// Update document attributes
        /// </summary>
        /// <param name="domrequest">DOMDocument with specified parameters, attributes, values
        /// <example>Update document attributes
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// string strXML = "
        /// <request>
        ///     <parameter name='listName'>Shared Documents</parameter>
        ///     <parameter name='fileRef'>sites/site/Shared Documents/test.doc</parameter>
        ///     <parameter name='attributes'>
        ///         <attribute name='Title'>new title</attribute>
        ///     </parameter>
        /// </request>"
        /// DOMDocument oDOM = new DOMDocument();
        /// oDOM.loadXML(strXML);
        /// breturn = oWS.UpdateDocumentAttributes(oDOM);
        /// </code>
        /// </example>
        /// </param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        public bool UpdateDocumentAttributes(DOMDocument domrequest)
        {
            bool lResponse = false;
            try
            {
                XmlNode XMLRequest = Handler.DOMDocument2XmlNode(domrequest);
                string listName = null;
                string fileRef = null;
                XmlNodeList attribs = null;
                if (XMLRequest.HasChildNodes)
                {
                    listName = XMLRequest.SelectSingleNode("/request/parameter[@name='listName']").InnerText;
                    fileRef = XMLRequest.SelectSingleNode("/request/parameter[@name='fileRef']").InnerText;
                    attribs = XMLRequest.SelectSingleNode("/request/parameter[@name='attributes']").ChildNodes;
                }


                string fields = null;

                foreach (XmlNode node in attribs)
                {
                    if (node.Attributes.Count == 2)
                    {
                        fields += "<Field Name='" + node.Attributes[0].Value + "' Property='" + node.Attributes[1].Value + "'>" + node.InnerText + "</Field>";
                    }
                    else
                    {
                        fields += "<Field Name='" + node.Attributes[0].Value + "'>" + node.InnerText + "</Field>";
                    }
                }


                string ID = GetDocumentID(listName, fileRef);

                Uri uri = new Uri(listsService.Url);
                string fileRef2 = uri.Scheme + "://" + uri.Authority + "/" + fileRef;
                
                string batch = "<Batch OnError='Continue' Properties='TRUE'>" +
                        "<Method ID='1' Cmd='Update'>" +
                        "<Field Name='ID'>" + ID + "</Field>" +
                        "<Field Name='FileRef'>" + fileRef2 + "</Field>" +
                        fields +
                        "</Method>" +
                        "</Batch>";

                Trace.WriteLine(batch);

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(batch);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(
                        listName,
                        updates
                        );
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }

        /// <summary>
        /// TODO:UpdateDocument
        /// </summary>
        /// <param name="listName">A string that contains either the title or GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <param name="attributes">.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        private bool UpdateDocument(string listName, string fileRef, DOMDocument attributes)
        {
            bool lResponse = false;
            try
            {
                string ID = GetDocumentID(listName, fileRef);
                //TODO:UpdateDocument parameters from xml
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='Update'>" +
                        "<Field Name='ID'>" + ID + "</Field>" +
                        "<Field Name='FileRef'>" + fileRef + "</Field>" +
                        "</Method>" +
                        "</Batch>";


                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(
                        listName,
                        updates
                        );
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            finally
            {
                //some finally code
            }
            return lResponse;
        }

        /// <summary>
        /// Return document attributes.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <returns>Document attributes assigned to DOMDocument.</returns>
        /// <example>Get document attributes
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domreturn = new DOMDocument();
        /// domreturn = oWS.GetDocumentAttributes("Shared Documents", "sites/site/Shared Documents/test.doc");
        /// </code>
        /// </example>
        public DOMDocument GetDocumentAttributes(string listName, string fileRef)
        {
            XmlNode lResponse;
            try
            {
                string queryStr = "<Query>" +
                    "<Where>" +
                    "<Eq>" +
                    "<FieldRef Name='FileRef' />" +
                    "<Value Type='Text'>" + fileRef + "</Value>" +
                    "</Eq>" +
                    "</Where>" +
                    "</Query>";
                XmlDocument queryDoc = new XmlDocument();
                queryDoc.LoadXml(queryStr);
                XmlNode queryNode = queryDoc;

                string viewFieldsStr = "<ViewFields>" +
                    "</ViewFields>";
                XmlDocument viewFieldsDoc = new XmlDocument();
                viewFieldsDoc.LoadXml(viewFieldsStr);
                XmlNode viewFieldsNode = viewFieldsDoc;

                XmlDocument xmldoc = new XmlDocument();
                XmlNode queryOptions = xmldoc.CreateNode(XmlNodeType.Element, "QueryOptions", "");

                string folderName = null;

                folderName = getFolderFromfileRef(listName, fileRef);

                if (folderName != null)
                {
                    queryOptions.InnerXml = "<Folder>" + folderName + "</Folder>";
                }
                else
                {
                }

                lResponse = priGetListItems(listName, null, queryNode, viewFieldsNode, "1", queryOptions, null);
            }
            catch (Exception ex)
            {
                lResponse = Handler.Exceptions(ex);
            }
            return Handler.XmlNode2DOMDocument(lResponse);
        }

        /// <summary>
        /// Adds an folder to the specified list.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="folderName">A string that contains the name of the folder.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Add folder
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.AddFolder("Shared Documents", "folder_name");
        /// </code>
        /// </example>
        public bool AddFolder(string listName, string folderName)
        {
            bool lResponse = false;
            try
            {
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='New'>" +
                        "<Field Name='ID'>New</Field>" +
                        "<Field Name='FSObjType'>1</Field>" +
                        "<Field Name='BaseName'>" + folderName + "</Field>" +
                        "</Method>" +
                        "</Batch>";

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(listName, updates);
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            return lResponse;
        }

        /// <summary>
        /// Delete the folder from the specified list item.
        /// </summary>
        /// <param name="listName">A string that contains either the title or GUID for the list.</param>
        /// <param name="fileRef">A string that contains URL path of file.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Delete folder
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.DeleteFolder("Shared Documents", "/sites/site/Shared Documents/folder_name");
        /// </code>
        /// </example>
        public bool DeleteFolder(string listName, string fileRef)
        {
            bool lResponse = false;
            try
            {
                string ID = GetDocumentID(listName, fileRef);
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='Delete'>" +
                        "<Field Name='ID'>" + ID + "</Field>" +
                        "<Field Name='FileRef'>" + fileRef + "</Field>" +
                        "</Method>" +
                        "</Batch>";

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(listName, updates);
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            return lResponse;
        }

        /// <summary>
        /// Adds an task to the specified list.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="taskTitle">A string that contains the title of the task.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Add task
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.AddTask("Tasks", "new task");
        /// </code>
        /// </example>
        public bool AddTask(string listName, string taskTitle)
        {
            bool lResponse = false;
            try
            {
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='New'>" +
                        "<Field Name='ID'>New</Field>" +
                        "<Field Name='Title'>" + taskTitle + "</Field>" +
                        "</Method>" +
                        "</Batch>";

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(listName, updates);
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            return lResponse;
        }

        /// <summary>
        /// Delete the task from the specified list.
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="taskID">A string that contains the ID of the item to delete.</param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Delete task
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// string idtask = null;
        /// idtask = oWSS.GetDocumentID("Tasks", "new task")
        /// if (idtask != null)
        /// {
        ///     breturn = oWS.DeleteTask("Tasks", idtask);
        /// }
        /// </code>
        /// </example>
        public bool DeleteTask(string listName, string taskID)
        {
            bool lResponse = false;
            try
            {
                string temp = "<Batch OnError='Continue'>" +
                        "<Method ID='1' Cmd='Delete'>" +
                        "<Field Name='ID'>" + taskID + "</Field>" +
                        "</Method>" +
                        "</Batch>";

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(temp);
                XmlNode updates = xmldoc;

                gXMLResponse = priUpdateListItems(listName, updates);
                lResponse = true;
            }
            catch (Exception ex)
            {
                gXMLResponse = Handler.Exceptions(ex);
            }
            return lResponse;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listName"></param>
        /// <param name="fileRef"></param>
        /// <returns></returns>
        public string getFolderFromfileRef(string listName, string fileRef)
        {
            DOMDocument listDOM = GetList(listName);
            //string tmplist = listDOM.firstChild.attributes.getNamedItem("RootFolder").text;
            string tmplist = listDOM.firstChild.attributes.getNamedItem("WebFullUrl").text;
            string tmpfileRef = "/" + fileRef;
            string tmpresult = null;
            tmplist = tmplist + "/";
            tmpfileRef = tmpfileRef.Replace(tmplist, "");
            String[] arr = tmpfileRef.Split('/');
            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (tmpresult == null)
                {
                    tmpresult = tmpresult + arr[i];
                }
                else
                {
                    tmpresult = tmpresult + "/" + arr[i];
                }
            }
            return tmpresult;
        }


    }
}
