using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using msxml3net;

namespace WSScom
{

    ///// <summary>
    ///// 
    ///// </summary>
    //[InterfaceTypeAttribute(ComInterfaceType.InterfaceIsDual)]
    //public interface IWSSWS
    //{
    //}

    /// <summary>
    /// Windows Sharepoint Services Web Services Class
    /// </summary>
    /// <remarks>Optionally init object with <see cref="Url"/>, <see cref="Useragent"/> and <see cref="InitCredential"/></remarks>
    //[Guid("19D2CA06-2403-4c04-AACB-4B2370A7F0A4")]
    //[ProgId("WSScom.WSSWS")]
    //public class WSSWS : IWSSWS
    [ClassInterface(ClassInterfaceType.AutoDual)] //NOTE: required for methods visibility in COM
    public partial class WSSWS
    {
        /// <summary>
        /// WSSWS constructor.
        /// </summary>
        /// <remarks>Optionally init object with <see cref="Url"/>, <see cref="Useragent"/> and <see cref="InitCredential"/></remarks>
        public WSSWS()
        {
            //constructor
        }

        private NetworkCredential sCredential = new NetworkCredential();
        private bool bInitsCredential = false;

        /// <summary>
        /// URL of WSS site.
        /// </summary>
        /// <value>URL of WSS site as string. Default value is <c>http://localhost/</c>.</value>
        /// <example>Set URL to <c>http://host/sites/name/</c>
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// oWS.Url = "http://host/sites/name/";
        /// </code>
        /// </example>
        public string Url
        {
            get
            {
                return sUrl;
            }
            set
            {
                //TODO:validate sUrl = value;
                //regexp validate http://host/ | http://host/site/ better            
                //throw new Exception("The method or operation is not implemented.");
                sUrl = value;
                bInitsUrl = true;
            }
        }
        private bool bInitsUrl = true;
        private string sUrl = "http://localhost/"; //default value

        /// <summary>
        /// Useragent of WSScom.
        /// </summary>
        /// <value>Useragent of WSScom as string. Default value is <c>WSScom</c>.</value>
        /// <example>Set Useragent to <c>WSScom_Agent</c>
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// oWS.Useragent = "WSScom_Agent";
        /// </code>
        /// </example>
        public string Useragent
        {
            get
            {
                return sUseragent;
            }
            set
            {
                sUseragent = value;
                bInitsUseragent = true;
            }
        }
        private bool bInitsUseragent = true;
        private string sUseragent = "WSScom"; //default value

        private XmlNode gXMLResponse;
        private DOMDocument gDOMResponse = new DOMDocument();
        /// <summary>
        /// Output of some methods as DOMDocument object.
        /// </summary>
        /// <value>Output of some methods as DOMDocument object.</value>
        /// <returns>DOMDocument object.</returns>
        /// <example>Get Response
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domresponse = oWS.Response;
        /// </code>
        /// </example>
        public DOMDocument Response
        {
            get
            {
                gDOMResponse.loadXML(gXMLResponse.OuterXml);
                return gDOMResponse;
            }
        }

        private XmlNode gXMLRequest;
        private DOMDocument gDOMRequest = new DOMDocument();
        /// <summary>
        /// Input for some methods as DOMDocument object.
        /// </summary>
        /// <value>Input of some methods as DOMDocument object.</value>
        /// <example>Set Request
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// DOMDocument domrequest = new DOMDocument();
        /// oWS.Request = domrequest;
        /// </code>
        /// </example>
        public DOMDocument Request
        {
            set
            {
                gDOMRequest.load(value);
                gXMLRequest = Handler.DOMDocument2XmlNode(gDOMRequest);
            }
        }

        /// <summary>
        /// Set credential for login to WSS.
        /// </summary>
        /// <param name="domain">A string that contains the domain name.</param>
        /// <param name="username">A string that contains the user name.</param>
        /// <param name="password">A string that contains the user password.</param>
        /// <remarks>Default is actually logged user.</remarks>
        /// <example>Set credential
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// oWS.InitCredential("DOMAIN_NAME", "USER_NAME", "USER_PASSWORD");
        /// </code>
        /// </example>
        public void InitCredential(string domain, string username, string password)
        {
            try
            {
                sCredential.Domain = domain;
                sCredential.UserName = username;
                sCredential.Password = password;
                bInitsCredential = true;
            }
            catch
            {
            }
        }

    }
}
