using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using msxml3net;
using WSS3WebServices.CopyWS;

namespace WSScom
{

    public partial class WSSWS
    {
        private Copy copyService = new Copy();
        private bool bInitcopyService = false;

        /// <summary>
        /// internal
        /// </summary>
        private void InitcopyService()
        {
            if (!bInitsCredential)
            {
                copyService.Credentials = CredentialCache.DefaultCredentials;
            }
            else
            {
                copyService.Credentials = sCredential;
            }
            if (bInitsUrl)
            {
                copyService.Url = sUrl + "_vti_bin/copy.asmx";
            }
            if (bInitsUseragent)
            {
                copyService.UserAgent = sUseragent;
            }
            bInitcopyService = true;
        }

        /// <summary>
        /// Adds an document to the specified list. 
        /// </summary>
        /// <param name="listName">A string that contains either the title or the GUID for the list.</param>
        /// <param name="fileRef">A string that contains the URL path for the document.</param>
        /// <param name="filePath">A string that contains local file path <c>c:\folder\file.doc</c></param>
        /// <returns><c>true</c> if the operation succeeded; otherwise, <c>false</c> with exception in <see cref="Response"/> property.</returns>
        /// <example>Add document
        /// <code language="C#">
        /// WSScom_Reference_Name.WSSWS oWS = new WSScom_Reference_Name.WSSWS();
        /// bool breturn = false;
        /// breturn = oWS.AddDocument("Shared Documents", "sites/site/Shared Documents/test.doc", "c:\test.doc");
        /// </code>
        /// </example>
        /// <remarks>after AddDocument use <see cref="UpdateDocumentAttributes"/> and set attribute "_CopySource" to empty string.</remarks>
        public bool AddDocument(string listName, string fileRef, string filePath)
        {

            if (!File.Exists(filePath))
                throw new Exception("Could not find file " + filePath);

            if (!bInitcopyService)
            {
                InitcopyService();
            }

            bool lResponse = false;

            string regexPattern = @"^(?<s1>(?<s0>[^:/\?#]+):)?(?<a1>"
                + @"//(?<a0>[^/\?#]*))?(?<p0>[^\?#]*)"
                + @"(?<q1>\?(?<q0>[^#]*))?"
                + @"(?<f1>#(?<f0>.*))?";
            Regex re = new Regex(regexPattern, RegexOptions.ExplicitCapture);
            Match m = re.Match(sUrl);

            String SitePath = m.Groups["s1"].Value + m.Groups["a1"].Value + "/";
            String filename = filePath;
            String[] CopyDestination = { String.Format("{0}", SitePath + fileRef) };
            FieldInformation FieldInfo = new FieldInformation();
            //FieldInfo.DisplayName = "Author";
            //FieldInfo.InternalName = "_CopySource";
            //FieldInfo.Type = WSCopy.FieldType.Text;
            //FieldInfo.Value = "Real Username";
            FieldInformation[] FieldInfoArray = { FieldInfo };
            CopyResult CopyResults = new CopyResult();
            CopyResult[] CopyResultArray = { CopyResults };

            try
            {

                byte[] FileDataBytes = File.ReadAllBytes(filename);
                uint CopyReturn = copyService.CopyIntoItems(filename, CopyDestination,
                FieldInfoArray, FileDataBytes, out CopyResultArray);
                if (CopyReturn == 0)
                {
                    if (CopyResultArray[0].ErrorMessage == null)
                    {
                        lResponse = true;
                        //String.Format("Copy operation completed: \"{0}\"", CopyResultArray[0].DestinationUrl);
                    }
                    else
                    {
                        gXMLResponse = Handler.WSSWSException("<CopyService>" + CopyResultArray[0].ErrorMessage + "</CopyService>");
                        //String.Format("Copy operation failed: \"{0}\"", CopyResultArray[0].ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                lResponse = false;
                gXMLResponse = Handler.Exceptions(ex);
                //String.Format("Something bad happened: {0}\n{1}", ex.Message, ex.InnerException);
            }
            return lResponse;
        }
    }
}
