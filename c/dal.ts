import { db } from '@arangodb';
import * as Graph from '@arangodb/general-graph';
import * as httpError from 'http-errors';
import { throwError } from './utils';

export class dal {
  static insert(collection: ArangoDB.Collection, data: any) {
    data.createdAt = new Date().valueOf();
    delete data.updatedAt;
    return Object.assign(data, collection.insert(data)) as
      | ArangoDB.Document
      | ArangoDB.Edge;
  }

  static update(
    collection: ArangoDB.Collection,
    selector:
      | ArangoDB.Document
      | ArangoDB.ObjectWithKey
      | ArangoDB.ObjectWithId
      | string,
    data: any,
  ) {
    delete data.createdAt;
    data.updatedAt = new Date().valueOf();
    return Object.assign(data, collection.update(selector, data)) as
      | ArangoDB.Document
      | ArangoDB.Edge;
  }

  static remove(
    collection: ArangoDB.Collection,
    selector:
      | ArangoDB.Document
      | ArangoDB.ObjectWithKey
      | ArangoDB.ObjectWithId
      | string,
  ) {
    return collection.remove(selector);
  }

  static getGraphCollection(
    graph,
    collectionName: string,
  ): ArangoDB.Collection {
    if (!graph[collectionName]) {
      throw new httpError.NotFound('ERROR_ARANGO_COLLECTION_NOT_FOUND');
    }
    return graph[collectionName];
  }

  static getGraph(graphName: string) {
    let graph;
    try {
      graph = Graph._graph(graphName);
    } catch (e) {
      // ERROR_GRAPH_NOT_FOUNDs
      throwError(e);
    }
    return graph;
  }

  static getDocumentId(collection: any, key: any) {
    return `${collection}/${key}`;
  }

  static aqlQuery(key: string, bindVars: object) {
    let queryDocument = db._collection('aql').document(key);
    return queryDocument && db._query(queryDocument.query, bindVars);
  }
}
