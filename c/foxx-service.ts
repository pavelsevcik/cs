type ResponseDataPropertyName = 'delete' | 'vertex' | 'edge' | 'path';

export function setResponse(
  res: Foxx.Response,
  name: ResponseDataPropertyName,
  data,
  code,
) {
  res.status(code);
  if (data._rev) {
    res.set('etag', data._rev);
  }
  res.json({
    error: false,
    [name]: data,
    code,
  });
}
