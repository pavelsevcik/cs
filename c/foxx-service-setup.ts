import { db } from '@arangodb';

function getCollectionName(collectionConfig: CollectionConfig) {
  return collectionConfig.context === CollectionContext.Module
    ? module.context.collectionName(collectionConfig.name)
    : collectionConfig.name;
}

export function setupFoxxService(config: FoxxServiceSetupConfig) {
  config.collections.forEach(collectionConfig => {
    let collectionName = getCollectionName(collectionConfig);
    if (!db._collection(collectionName)) {
      switch (collectionConfig.type) {
        case CollectionType.Document:
          db._createDocumentCollection(collectionName);
          break;
        case CollectionType.Edge:
          db._createEdgeCollection(collectionName);
          break;
      }
    } else if (module.context.isProduction) {
      console.warn(
        `[setupFoxxService] collection ${collectionName} already exists. Leaving it untouched.`,
      );
    }
  });
  config.documents.forEach(payload => {
    let collectionName = payload._id.split('/').shift() || '';
    let collection = db._collection(module.context.collectionName(collectionName)) || db._collection(collectionName);
    if (!collection) {
      console.error(
        `[setupFoxxService] collection ${collectionName} does not exists. Cannot create ${payload._id}.`
      );
    } else {
      payload._key = payload._id.split('/').pop();
      let document = collection.exists(payload._key);
      !document && collection.save(payload, { returnNew: true });
    }
  })
}

export function teardownFoxxService(config: FoxxServiceSetupConfig) {
  config.collections.forEach(collectionConfig => {
    let collectionName =
      collectionConfig.context === CollectionContext.Module
        ? module.context.collectionName(collectionConfig.name)
        : collectionConfig.name;
    let collection =
      collectionConfig.dropWhenTeardown && db._collection(collectionName);
    if (collection) {
      if (collection.count()) {
        console.warn(
          `[teardownFoxxService] collection ${collectionName} not empty. Leaving it untouched.`,
        );
      } else {
        db._drop(collectionName);
      }
    }
  });
}



export interface FoxxServiceSetupConfig {
  collections: CollectionConfig[];
  documents: DocumentConfig[];
}

interface DocumentConfig {
  _id: string;
  edgeDefinitions?: GraphEdgeDefinition[];
  orphanCollections?: GraphOrphanCollection[];
  [property: string]: any;
}

interface GraphEdgeDefinition {
  collection: string;
  from: string[];
  to: string[];
}

interface GraphOrphanCollection {

}

interface CollectionConfig {
  name: string;
  context: CollectionContext;
  type: CollectionType;
  dropWhenTeardown: boolean;
}

export enum CollectionContext {
  Db,
  Module,
}

export enum CollectionType {
  Document,
  Edge,
}
