﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.SharePoint;
using SharePoint.Extensions;
using SharePoint.Extensions.SystemEx;
using SharePoint.Extensions.SystemEx.Xml;

namespace SharePoint.Extensions
{
    public class SPListSerializable
    {
        private ArrayList viewsList;
        private ArrayList fieldsList;
        private ArrayList contenttypesList;

        [XmlIgnore]
        public SPList SPList { get; private set; } // end SPList

        public string Title { get; set; } // end Title
        public string Description { get; set; } // end Description
        public string Url { get; set; } // end Url
        public bool EnableVersioning { get; set; } // end EnableVersioning
        public bool EnableMinorVersions { get; set; } // end EnableMinorVersions
        public bool EnableFolderCreation { get; set; } // end EnableFolderCreation
        public int MajorVersionLimit { get; set; } // end MajorVersionLimit
        public int MajorWithMinorVersionsLimit { get; set; } // end MajorWithMinorVersionsLimit
        public bool ForceCheckout { get; set; } // end ForceCheckout
        public string DefaultView { get; set; } // end DefaultView
        [XmlIgnore]
        public string FieldsSchemaXml { get; set; } // end FieldsSchemaXml
        [XmlArray("Views")]
        [XmlArrayItem("View")]
        public SPViewSerializable[] Views
        {
            get
            {
                SPViewSerializable[] items = new SPViewSerializable[viewsList.Count];
                viewsList.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                SPViewSerializable[] items = (SPViewSerializable[])value;
                viewsList.Clear();
                foreach (SPViewSerializable item in items)
                    viewsList.Add(item);
            }
        } // end Views
        [XmlArray("Fields")]
        [XmlArrayItem("Field")]
        public SPFieldSerializable[] Fields
        {
            get
            {
                SPFieldSerializable[] items = new SPFieldSerializable[fieldsList.Count];
                fieldsList.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                SPFieldSerializable[] items = (SPFieldSerializable[])value;
                fieldsList.Clear();
                foreach (SPFieldSerializable item in items)
                    fieldsList.Add(item);
            }
        } // end Fields

        [XmlArray("ContentTypes")]
        [XmlArrayItem("ContentType")]
        public string[] ContentTypes
        {
            get
            {
                string[] items = new string[contenttypesList.Count];
                contenttypesList.CopyTo(items);
                return items;
            }
            set
            {
                if (value == null) return;
                string[] items = (string[])value;
                contenttypesList.Clear();
                foreach (string item in items)
                    contenttypesList.Add(item);
            }
        } // end ContentTypes

        public SPListSerializable()
        {
            viewsList = new ArrayList();
            fieldsList = new ArrayList();
            contenttypesList = new ArrayList();
        } // end SPListSerializable

        public SPListSerializable(SPList list)
        {
            viewsList = new ArrayList();
            fieldsList = new ArrayList();
            contenttypesList = new ArrayList();

            SPList = list;

            Title = list.Title;
            Description = list.Description;
            Url = list.RootFolder.Url;

            EnableVersioning = list.EnableVersioning;
            EnableMinorVersions = list.EnableMinorVersions;
            EnableFolderCreation = list.EnableFolderCreation;
            MajorVersionLimit = list.MajorVersionLimit;
            MajorWithMinorVersionsLimit = list.MajorWithMinorVersionsLimit;
            ForceCheckout = list.ForceCheckout;
            FieldsSchemaXml = list.Fields.SchemaXml;
            DefaultView = list.DefaultView.Title;

            foreach (SPView view in list.Views)
            {
                if (!view.PersonalView)
                {
                    viewsList.Add(new SPViewSerializable(view));
                }
            }

            foreach (SPField field in list.Fields)
            {
                fieldsList.Add(new SPFieldSerializable(field));
            }

            foreach (SPContentType ct in list.ContentTypes)
            {
                SPContentTypeId spctid = ct.Id;
                if (ct.Parent.IsNotNull())
                {
                    if (ct.IsNotNewerThanParent())
                    {
                        spctid = ct.Parent.Id;
                    }
                    //else
                    //{
                    //    if (ct.Parent.IsBuiltIn())
                    //    {
                    //        spctid = ct.Parent.Id;
                    //    }
                    //}
                }

                contenttypesList.Add(spctid.ToString());
            }
        } // end SPListSerializable

        /// <summary>
        /// DecodeFriendlyAndRemoveNonAlphaNumericChars on non BuiltIn Fields
        /// </summary>
        public void FixStaticAndInternalNamesOfFields()
        {
            // fix field staticname, internal names
            foreach (SPFieldSerializable fieldser in this.Fields)
            {
                if (!fieldser.IsBuiltIn)
                {
                    string internalName = fieldser.InternalName;
                    string staticName = fieldser.StaticName;
                    string internalNameDecodedCleaned = fieldser.InternalNameDecodedCleaned;
                    string staticNameDecodedCleaned = fieldser.StaticNameDecodedCleaned;

                    fieldser.SchemaXml = fieldser.SchemaXml.Replace("Name=\"" + internalName + "\"", "Name=\"" + internalNameDecodedCleaned + "\"");
                    fieldser.SchemaXml = fieldser.SchemaXml.Replace("StaticName=\"" + staticName + "\"", "StaticName=\"" + staticNameDecodedCleaned + "\"");
                    fieldser.InternalName = fieldser.InternalNameDecodedCleaned;
                    fieldser.StaticName = fieldser.StaticNameDecodedCleaned;

                    ReplaceFieldNamesInSerializedViews(internalName, internalNameDecodedCleaned);
                }
            }
        } // end FixStaticAndInternalNamesOfFields

        public void ReplaceFieldNamesInSerializedViews(string internalName, string newName)
        {
            foreach (SPViewSerializable viewser in this.Views)
            {
                if (viewser.Query.IsNotNull())
                {
                    viewser.Query = viewser.Query.Replace("=\"" + internalName + "\"", "=\"" + newName + "\"");
                }
                if (viewser.ViewFields.IsNotNull())
                {
                    viewser.ViewFields = viewser.ViewFields.Replace("=\"" + internalName + "\"", "=\"" + newName + "\"");
                }
                if (viewser.SchemaXml.IsNotNull())
                {
                    viewser.SchemaXml = viewser.SchemaXml.Replace("=\"" + internalName + "\"", "=\"" + newName + "\"");
                }
                if (viewser.HtmlSchemaXml.IsNotNull())
                {
                    viewser.HtmlSchemaXml = viewser.HtmlSchemaXml.Replace("=\"" + internalName + "\"", "=\"" + newName + "\"");
                }
                if (viewser.ViewFieldCollection.Contains(internalName))
                {
                    int indexof = viewser.ViewFieldCollection.IndexOf(internalName);
                    viewser.ViewFieldCollection.Remove(internalName);
                    viewser.ViewFieldCollection.Insert(indexof, newName);
                }
            }
        } // end ReplaceFieldNamesInSerializedViews

        public void UpdateList(SPList list, bool throwexception)
        {
            if (list == null)
            {
                throw new ArgumentNullException("Parameter SPList list cannot be a null.");
            }

            try
            {
                Trace.Write("UpdateList:{0}".DoFormat(list.Title));

                Trace.Write("Title:{0}".DoFormat(this.Title));
                // Title
                list.Title = this.Title;

                Trace.Write("Description:{0}".DoFormat(this.Description));
                // Title
                list.Description = this.Description;

                Trace.Write("Versioning:{0}".DoFormat(this.EnableVersioning));
                // Versioning
                list.EnableVersioning = this.EnableVersioning;
                if (this.EnableVersioning)
                {
                    Trace.Write("MajorVersionLimit:{0}".DoFormat(this.MajorVersionLimit));
                    list.MajorVersionLimit = this.MajorVersionLimit;
                }

                if (list.BaseType == SPBaseType.DocumentLibrary)
                {
                    Trace.Write("EnableMinorVersions:{0}".DoFormat(this.EnableMinorVersions));
                    list.EnableMinorVersions = this.EnableMinorVersions;
                    if (this.EnableMinorVersions)
                    {
                        Trace.Write("MajorWithMinorVersionsLimit:{0}".DoFormat(this.MajorWithMinorVersionsLimit));
                        list.MajorWithMinorVersionsLimit = this.MajorWithMinorVersionsLimit;
                    }
                }

                // Checkout
                Trace.Write("ForceCheckout:{0}".DoFormat(this.ForceCheckout));
                list.ForceCheckout = this.ForceCheckout;

                // EnableFolderCreation
                Trace.Write("EnableFolderCreation:{0}".DoFormat(EnableFolderCreation));
                list.EnableFolderCreation = EnableFolderCreation;


                Trace.Write("Fields");
                // Fields
                //                foreach (SPFieldSerializable fieldS in this.Fields)
                //                {
                //                    // if field id or static name not exists on list
                //                    try
                //                    {
                //                        if (!(list.Fields.ContainsField(fieldS.Id) | list.Fields.ContainsField(fieldS.StaticName)))
                //                        {
                //                            //                            // if field exists as site column on rootweb add as reference
                //                            //                            // otherwise add as xml
                //                            //#warning rootweb, web, list by scope of field, now only rootweb based/required
                //                            //                            SPWeb rootweb = list.ParentWeb.GetRootWeb();
                //                            //                            if (rootweb.Fields.ContainsField(fieldS.Id))
                //                            //                            {
                //                            //                                Trace.Write("Fields.Add(rootweb.Fields.GetField({0}))".DoFormat(fieldS.Id));
                //                            //                                list.Fields.Add(rootweb.Fields.GetField(fieldS.Id));
                //                            //                            }
                //                            //                            else
                //                            //                            {
                //                            //                                string schemaXml = fieldS.SchemaXml;
                //                            //                                if (fieldS.Type == SPFieldType.Lookup)
                //                            //                                {
                //                            //                                    // load schem xml, update list id if exists on
                //                            //                                    XmlDocument xmldoc = new XmlDocument();
                //                            //                                    xmldoc.LoadXml(schemaXml);
                //                            //                                    XmlElement schemaXmlElement = xmldoc.DocumentElement;
                //                            //                                    string listUrl = schemaXmlElement.AttributeValue("List");
                //                            //                                    if (listUrl.IsNotNullOrEmpty())
                //                            //                                    {
                //                            //                                        schemaXmlElement.UpdateAttribute("List", rootweb.GetListByUrl(listUrl, true).ID.ToString());
                //                            //                                    }
                //                            //                                }

                //                            //                                Trace.Write("Fields.AddFieldAsXml:{0}".DoFormat(schemaXml));
                //                            //                                list.Fields.AddFieldAsXml(schemaXml);

                //                            //                                if (SPBuiltInFieldId.Contains(fieldS.Id))
                //                            //                                {
                //                            //                                }
                //                            //                            }
                //                        }
                //                        else
                //                        {
                //                            if (list.Fields.ContainsField(fieldS.Id))
                //                            {
                //                                SPField field = list.Fields[fieldS.Id];

                //#warning for lookup cause err in listid
                //                                //XmlDocument xmldoc = new XmlDocument();
                //                                //xmldoc.LoadXml(field.SchemaXml);
                //                                //XmlElement fieldxmlel = xmldoc.DocumentElement;
                //                                //fieldxmlel.UpdateAttribute("DisplayName", fieldS.Title);
                //                                //fieldxmlel.UpdateAttribute("Description", fieldS.Description);
                //                                //string schemaXml = xmldoc.OuterXml;
                //                                //Trace.Write("field.SchemaXml.Update:{0}".DoFormat(schemaXml));
                //                                //field.SchemaXml = schemaXml;

                //                                if (field.Title != fieldS.Title
                //                                    | field.Description != fieldS.Description)
                //                                {
                //                                    Trace.Write("field.Update:\"{0}\" Title \"{1}\", Description \"{2}\"".DoFormat(field.Title, fieldS.Title, fieldS.Description));
                //                                    field.Title = fieldS.Title;
                //                                    field.Description = fieldS.Description;
                //                                    field.Update(false);
                //                                }
                //                            }
                //                        }
                //                    }
                //                    catch
                //                    {
                //                        //TODO: log
                //                    }
                //                }

                Trace.Write("ContentTypes");
                // ContentTypes
                foreach (string ctid in this.contenttypesList)
                {
                    SPContentTypeId ctId = new SPContentTypeId(ctid);
                    if (list.ContentTypes.BestMatch(ctId).IsNull())
                    {
                        SPContentType spct = list.ParentWeb.GetRootWeb().ContentTypes[ctId];
                        if (spct.IsNotNull())
                        {
                            Trace.Write("ContentTypes.Add:{0}".DoFormat(spct.Name));
                            list.ContentTypes.Add(spct);
                        }
                    }
                }



                Trace.Write("Views");
                // Views
                foreach (SPViewSerializable viewS in this.Views)
                {
                    if (list.Views.Exists(viewS.Title))
                    {
                        SPView view = list.Views[viewS.Title];
                        Trace.Write("UpdateView:{0}".DoFormat(viewS.Title));
                        viewS.UpdateView(view, throwexception);
                    }
                    else
                    {
                        Trace.Write("CreateView:{0}".DoFormat(viewS.Title));
                        viewS.CreateView(list, throwexception);
                    }
                }
                try
                {
                    if (DefaultView.IsNotNullOrEmpty())
                    {
                        var defaultview = list.Views[DefaultView];
                        defaultview.DefaultView = true;
                        defaultview.MobileDefaultView = true;
                        defaultview.Update();
                    }
                }
                catch (Exception ex)
                {
                    Trace.Write("ERROR:DefaultView {0} wasn't successfully set:{1}".DoFormat(DefaultView, ex));
                }

                //remove cts
                List<SPContentTypeId> ctidsforremove = new List<SPContentTypeId>();
                // remove unneeded CT usually default CT
                foreach (SPContentType ct in list.ContentTypes)
                {
                    bool remove = true;
                    foreach (string ctid in this.contenttypesList)
                    {
                        if (ct.Id.IsChildOf(new SPContentTypeId(ctid)))
                        {
                            remove = false;
                            continue;
                        }
                    }
                    if (remove)
                    {
                        ctidsforremove.Add(ct.Id);
                    }
                }
                foreach (SPContentTypeId ct in ctidsforremove)
                {
                    try
                    {
                        Trace.Write("ContentTypes.Delete:{0}".DoFormat(ct));
                        if (list.ContentTypes.Count > 1)
                        {
                            list.ContentTypes.Delete(ct);
                        }
                        else
                        {
                            Trace.Write("WARNING:ContentTypes.Delete: content type cannot be deleted is its last in list.");
                        }
                    }
                    catch (Exception ex)
                    {
                        Trace.Write("ERROR:ContentTypes.Delete:{0}".DoFormat(ex));
                    }
                }

                list.Update();
            }
            catch
            {
                if (throwexception)
                {
                    throw;
                }
            }
        } // end UpdateList
    } // end class SPListSerializable

    public static class SPListEx
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ValueExists(this SPField field, string value)
        {
            return (field.ValueCount(value) > 0);
        } // end ValueExists

        /// <summary>
        ///
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int ValueCount(this SPField field, string value)
        {
            var q = new SPQuery();
            q.Query = "<Where><Eq><FieldRef Name='{0}' /><Value Type='{1}'>{2}</Value></Eq></Where>".DoFormat(field.InternalName, field.TypeAsString, value);
            q.ViewFields = "<FieldRef Name='ID' />";
            return field.ParentList.GetItems(q).Count;
        } // end ValueCount

        /// <summary>
        ///
        /// </summary>
        /// <param name="list">this SPList</param>
        /// <returns>SPListItem</returns>
        public static SPListItem AddListItem(this SPList list)
        {
            //for performance list.Items.Add() loads all items first!!!
            SPQuery query = new SPQuery();
            query.Query = "<Where><IsNull><FieldRef Name='ID' /></IsNull></Where>";
            SPListItem listitem = list.GetItems(query).Add();
            return listitem;
        } // end AddListItem
    } // end class SPListEx
} // end namespace SharePoint.Extensions
