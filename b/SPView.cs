﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using SharePoint.Extensions;
using SharePoint.Extensions.SystemEx;
using SharePoint.Extensions.Utilities;

namespace SharePoint.Extensions
{
    public class SPViewSerializable
    {
        [XmlIgnore]
        public SPView View { get; private set; } // end View

        public string Aggregations { get; set; } // end Aggregations
        public string AggregationsStatus { get; set; } // end AggregationsStatus
        public string ContentTypeId { get; set; } // end ContentTypeId
        public string Title { get; set; } // end Title
        public string BaseViewID { get; set; } // end BaseViewID
        public bool DefaultView { get; set; } // end DefaultView
        public bool Hidden { get; set; } // end Hidden
        public bool IncludeRootFolder { get; set; } // end IncludeRootFolder
        public bool Paged { get; set; } // end Paged
        public uint RowLimit { get; set; } // end RowLimit
        public string RowLimitExceeded { get; set; } // end RowLimitExceeded
        public string Type { get; set; } // end Type
        public string Url { get; set; } // end Url
        public string GroupByFooter { get; set; } // end GroupByFooter
        public string GroupByHeader { get; set; } // end GroupByHeader
        public string Query { get; set; } // end Query
        public string ViewBody { get; set; } // end ViewBody
        public string ViewData { get; set; } // end ViewData
        public string ViewEmpty { get; set; } // end ViewEmpty
        public string ViewFooter { get; set; } // end ViewFooter
        public string ViewHeader { get; set; } // end ViewHeader
        public string ViewFields { get; set; } // end ViewFields
        public string SchemaXml { get; set; } // end SchemaXml
        public string HtmlSchemaXml { get; set; } // end HtmlSchemaXml
        public StringCollection ViewFieldCollection { get; set; } // end ViewFieldCollection

        public SPViewSerializable()
        { } // end SPViewSerializable

        public SPViewSerializable(SPView view)
        {
            View = view;

            Aggregations = view.Aggregations;
            AggregationsStatus = view.AggregationsStatus;
            ContentTypeId = view.ContentTypeId.ToString();

            Title = view.Title;
            BaseViewID = view.BaseViewID;
            DefaultView = view.DefaultView;
            Hidden = view.Hidden;
            IncludeRootFolder = view.IncludeRootFolder;
            Paged = view.Paged;
            RowLimit = view.RowLimit;
            RowLimitExceeded = view.RowLimitExceeded;
            Type = view.Type;
            Url = view.Url;

            GroupByFooter = view.GroupByFooter;
            GroupByHeader = view.GroupByHeader;
            Query = view.Query;
            ViewBody = view.ViewBody;
            ViewData = view.ViewData;
            ViewEmpty = view.ViewEmpty;
            ViewFooter = view.ViewFooter;
            ViewHeader = view.ViewHeader;
            ViewFields = view.ViewFields.SchemaXml;
            SchemaXml = view.SchemaXml;
            HtmlSchemaXml = view.HtmlSchemaXml;

            ViewFieldCollection = view.ViewFields.ToStringCollection();
        } // end SPViewSerializable

        public void UpdateView(SPView view, bool throwexception)
        {
            UpdateView(view, HtmlSchemaXml, throwexception);
        } // end UpdateView

        public static void UpdateView(SPView view, string htmlschemaxml, bool throwexception)
        {
            if (htmlschemaxml.IsNullOrEmpty())
            {
                Trace.Write("HtmlSchemaXml cannot be null.");
                throw new ArgumentNullException("HtmlSchemaXml cannot be null.");
            }

            try
            {
                Trace.Indent();
                Trace.Write("UpdateView:".DoFormat(view.Title));
                // create a temporary view that we can copy from.
                XmlDocument viewDoc = new XmlDocument();
                viewDoc.LoadXml(htmlschemaxml);
                SPView tempView = new SPView(view.ParentList, viewDoc);

                // reset all the fields by deleting the existing fields and adding them back in from the new version of the view
                Trace.Write("ViewFields.DeleteAll()");
                view.ViewFields.DeleteAll();
                SPFieldCollection fieldCollection = view.ParentList.Fields;
                foreach (string s in tempView.ViewFields)
                {
                    SPField field = null;
                    Trace.Write("ContainsField:" + s);
                    if (fieldCollection.ContainsField(s))
                    {
                        Trace.Write("GetField:" + s);
                        field = fieldCollection.GetField(s);
                    }
                    if (field != null)
                    {
                        Trace.Write("ViewFields.Add:" + field.Id.ToString());
                        view.ViewFields.Add(field);
                    }
                }

                Trace.Write("view.other settings");
                // Now set all the other properties of the view using tempView as the source.
                view.GroupByHeader = tempView.GroupByHeader;
                view.GroupByFooter = tempView.GroupByFooter;
                view.Formats = tempView.Formats;
                view.IncludeRootFolder = tempView.IncludeRootFolder;
                view.Paged.TrySet(tempView.Paged);
                view.Query = tempView.Query;
                view.RowLimit = tempView.RowLimit;
                view.RowLimitExceeded = tempView.RowLimitExceeded;
                view.Scope = tempView.Scope;
                view.Title = tempView.Title;
                view.Toolbar = tempView.Toolbar;
                view.ViewBody = tempView.ViewBody;
                view.ViewData = tempView.ViewData;
                view.ViewEmpty = tempView.ViewEmpty;
                view.ViewFooter = tempView.ViewFooter;
                view.ViewHeader = tempView.ViewHeader;
                view.Aggregations = tempView.Aggregations;
                view.AggregationsStatus.TrySet(tempView.AggregationsStatus);

                // Save the changes to the view (note that we don't have to save the web part again as
                // the web part just links to this hidden view on the list).
                Trace.Write("view.Update()");
                view.Update();
            }
            catch
            {
                if (throwexception)
                {
                    //TODO: log
                    throw;
                }
            }
            finally
            {
                Trace.Unindent();
            }
        } // end UpdateView

        public void CreateView(SPList list, bool throwexception)
        {
            CreateView(list, HtmlSchemaXml, Url, throwexception);
        } // end CreateView

        public static void CreateView(SPList list, string htmlschemaxml, string viewFileName, bool throwexception)
        {
            if (htmlschemaxml.IsNullOrEmpty())
            {
                Trace.Write("HtmlSchemaXml cannot be null.");
                throw new ArgumentNullException("HtmlSchemaXml cannot be null.");
            }

            try
            {
                Trace.Write("CreateView on list " + list.Title);
                // create the view from HtmlSchemaXml
                XmlDocument viewDoc = new XmlDocument();
                viewDoc.LoadXml(htmlschemaxml);
                SPView view = new SPView(list, viewDoc);
                Trace.Write("new SPView " + view.Title);

                string urlfile = SPUtility.GetUrlFileName(viewFileName);
                if (urlfile.IsNullOrEmpty())
                {
                    urlfile = SPStringUtilityEx.DecodeFriendlyAndRemoveNonAlphaNumericChars(view.Title);
                }

                Trace.Write("new SPView.Clone " + view.ServerRelativeUrl);
                // clone the view in order to call update which then adds the view to the list.
                SPView newView = view.Clone(urlfile.Replace(".aspx", ""), view.RowLimit, view.Paged, view.DefaultView);
                newView.Title = view.Title;
                newView.Update();
            }
            catch
            {
                if (throwexception)
                {
                    throw;
                }
            }
        } // end CreateView
    } // end class SPViewSerializable

    public static class SPViewEx
    {
        /// <summary>
        /// Get SPView
        /// </summary>
        /// <param name="guid">Guid</param>
        /// <returns>SPView or null if view not exists</returns>
        public static SPView GetView(this SPViewCollection viewcollection, Guid guid)
        {
            if (viewcollection == null)
                throw new ArgumentNullException("viewcollection");

            SPView view = null;
            for (int i = 0; i < viewcollection.Count; i++)
            {
                if (viewcollection[i].ID == guid)
                {
                    view = viewcollection[i];
                    break;
                }
            }
            return view;
        } // end GetView

        /// <summary>
        /// Get SPView
        /// </summary>
        /// <param name="name">Guid</param>
        /// <returns>SPView or null if view not exists</returns>
        public static SPView GetViewByUrl(this SPViewCollection viewcollection, string url)
        {
            if (viewcollection == null)
                throw new ArgumentNullException("viewcollection");

            SPView view = null;
            for (int i = 0; i < viewcollection.Count; i++)
            {
                if (viewcollection[i].Url.EndsWith(url))
                {
                    view = viewcollection[i];
                    break;
                }
            }
            return view;
        } // end GetViewByUrl
    } // end class SPViewEx
} // end namespace SharePoint.Extensions
