﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Diagnostics;
using SharePoint.Extensions.Utilities;

namespace SharePoint.Extensions
{
    [DebuggerDisplay("{StaticName}, {Title}, {Id}")]
    public class SPFieldSerializable
    {
        public SPFieldSerializable()
        { }

        public SPFieldSerializable(SPField field)
        {
            Field = field;

            AggregationFunction = field.AggregationFunction;
            AllowDeletion = field.AllowDeletion;
            AuthoringInfo = field.AuthoringInfo;
            CanBeDeleted = field.CanBeDeleted;
            CanToggleHidden = field.CanToggleHidden;
            DefaultFormula = field.DefaultFormula;
            DefaultValue = field.DefaultValue;
            Description = field.Description;
            Direction = field.Direction;
            DisplaySize = field.DisplaySize;
            FieldReferences = field.FieldReferences;
            FieldTypeDefinition = field.FieldTypeDefinition;
            FieldValueType = field.FieldValueType;
            Filterable = field.Filterable;
            FilterableNoRecurrence = field.FilterableNoRecurrence;
            FromBaseType = field.FromBaseType;
            Group = field.Group;
            Hidden = field.Hidden;
            Id = field.Id;
            IMEMode = field.IMEMode;
            Indexed = field.Indexed;
            InternalName = field.InternalName;
            JumpToField = field.JumpToField;
            NoCrawl = field.NoCrawl;
            PIAttribute = field.PIAttribute;
            PITarget = field.PITarget;
            PrimaryPIAttribute = field.PrimaryPIAttribute;
            PrimaryPITarget = field.PrimaryPITarget;
            PushChangesToLists = field.PushChangesToLists;
            ReadOnlyField = field.ReadOnlyField;
            RelatedField = field.RelatedField;
            Reorderable = field.Reorderable;
            Required = field.Required;
            Scope = field.Scope;
            Sealed = field.Sealed;
            ShowInDisplayForm = field.ShowInDisplayForm;
            ShowInEditForm = field.ShowInEditForm;
            ShowInListSettings = field.ShowInListSettings;
            ShowInNewForm = field.ShowInNewForm;
            ShowInVersionHistory = field.ShowInVersionHistory;
            ShowInViewForms = field.ShowInViewForms;
            SchemaXml = field.SchemaXml;
            Sortable = field.Sortable;
            SourceId = field.SourceId;
            StaticName = field.StaticName;
            Title = field.Title;
            TranslationXml = field.TranslationXml;
            Type = field.Type;
            TypeAsString = field.TypeAsString;
            TypeDisplayName = field.TypeDisplayName;
            TypeShortDescription = field.TypeShortDescription;
            UsedInWebContentTypes = field.UsedInWebContentTypes;
            Version = field.Version;
            XPath = field.XPath;

            IsBuiltIn = SPBuiltInFieldId.Contains(field.Id);
            if (!IsBuiltIn)
            {
                string staticName = string.Empty;
                string internalName = string.Empty;
                field.GetStaticNameAndInternalNameFromTitle(out staticName, out internalName);
                StaticNameDecodedCleaned = staticName;
                InternalNameDecodedCleaned = internalName;
            }


        }

        [XmlIgnore]
        public SPField Field { get; set; } // end List
        public string InternalNameDecodedCleaned { get; set; }
        public string StaticNameDecodedCleaned { get; set; }

        public bool IsBuiltIn { get; set; }
        public string AggregationFunction { get; set; }
        public bool? AllowDeletion { get; set; }
        //
        // Summary:
        //     Gets the descriptive string used in pages for editing fields to identify
        //     the field and its purpose.
        //
        // Returns:
        //     A string that contains the description.
        public string AuthoringInfo { get; set; }
        public bool CanBeDeleted { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether the column can be hidden through
        //     the user interface.
        //
        // Returns:
        //     true if the column can be hidden; otherwise, false .
        public bool CanToggleHidden { get; set; }
        //
        // Summary:
        //     Gets or sets the default formula for a calculated field.
        //
        // Returns:
        //     A string that contains the formula.
        public string DefaultFormula { get; set; }
        //
        // Summary:
        //     Gets or sets the default value for a field.
        //
        // Returns:
        //     A string that contains the default value.
        public virtual string DefaultValue { get; set; }
        //
        // Summary:
        //     Gets or sets the description for a field.
        //
        // Returns:
        //     A string that contains the description.
        public string Description { get; set; }
        //
        // Summary:
        //     Gets or sets the direction of the reading order for the field.
        //
        // Returns:
        //     A string that contains LTR if the reading order is left-to-right, RTL if
        //     it is right-to-left, or none.
        public string Direction { get; set; }
        //
        // Summary:
        //     Gets or sets the display size for the field.
        //
        // Returns:
        //     A string that contains the display size.
        public string DisplaySize { get; set; }
        //
        // Summary:
        //     Gets a string array that contains the names of fields referenced in a computed
        //     field.
        //
        // Returns:
        //     A string array that contains the names of the fields.
        public string[] FieldReferences { get; set; }
        [XmlIgnore]
        public SPFieldTypeDefinition FieldTypeDefinition { get; set; }
        [XmlIgnore]
        public virtual Type FieldValueType { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether the field can be filtered.
        //
        // Returns:
        //     true if the field can be filtered; otherwise, false.
        public virtual bool Filterable { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether a filter can be created on a
        //     field in a view that does not expand recurring events.
        //
        // Returns:
        //     true if a filter can be created on the field in a view that does not expand
        //     recurring events; otherwise, false.
        public virtual bool FilterableNoRecurrence { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether the field derives from a base
        //     field type.
        //
        // Returns:
        //     true if the field derives from a base field type; otherwise, false.
        public bool FromBaseType { get; set; }
        public string Group { get; set; }
        //
        // Summary:
        //     Gets or sets a Boolean value that specifies whether the field is displayed
        //     in the list.
        //
        // Returns:
        //     true if the field is hidden; otherwise, false.
        public bool Hidden { get; set; }
        public Guid Id { get; set; }
        //
        // Summary:
        //     Gets or sets the Input Method Editor (IME) mode bias to use for the field.
        //     The IME allows for conversion of keystrokes between languages when one writing
        //     system has more characters than can be encoded for the given keyboard.
        //
        // Returns:
        //     A string that specifies the mode.
        public virtual string IMEMode { get; set; }
        public bool Indexed { get; set; }
        //
        // Summary:
        //     Gets the internal name used for the field.
        //
        // Returns:
        //     A string that contains the internal name.
        public string InternalName { get; set; }
        public string JumpToField { get; set; }
        public virtual bool NoCrawl { get; set; }
        public string PIAttribute { get; set; }
        public string PITarget { get; set; }
        public string PrimaryPIAttribute { get; set; }
        public string PrimaryPITarget { get; set; }
        public bool PushChangesToLists { get; set; }
        //
        // Summary:
        //     Gets or sets a Boolean value that specifies whether values in the field can
        //     be modified.
        //
        // Returns:
        //     true if the field can be modified; otherwise, false.
        public bool ReadOnlyField { get; set; }
        public string RelatedField { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether values in the field can be reordered.
        //
        // Returns:
        //     true if values can be reordered; otherwise, false.
        public bool Reorderable { get; set; }
        //
        // Summary:
        //     Gets or sets a Boolean value that determines whether the field requires values.
        //
        // Returns:
        //     true if the field requires values; otherwise, false.
        public bool Required { get; set; }
        public string Scope { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that indicates whether other fields can be derived from
        //     the field.
        //
        // Returns:
        //     true if other fields can be derived from the field; otherwise, false.
        public bool Sealed { get; set; }
        public bool? ShowInDisplayForm { get; set; }
        public bool? ShowInEditForm { get; set; }
        public bool? ShowInListSettings { get; set; }
        public bool? ShowInNewForm { get; set; }
        public bool ShowInVersionHistory { get; set; }
        public bool? ShowInViewForms { get; set; }
        //
        // Summary:
        //     Gets or sets the schema that defines the field.
        //
        // Returns:
        //     A string in Collaborative Application Markup Language (CAML) that contains
        //     the schema.
        public string SchemaXml { get; set; }
        //
        // Summary:
        //     Gets a Boolean value that determines whether the field can be sorted.
        //
        // Returns:
        //     true if the field can be sorted; otherwise, false.
        public virtual bool Sortable { get; set; }
        public string SourceId { get; set; }
        public string StaticName { get; set; }
        //
        // Summary:
        //     Gets or sets the display name for the field.
        //
        // Returns:
        //     A string that contains the display name.
        public string Title { get; set; }
        public string TranslationXml { get; set; }
        //
        // Summary:
        //     Gets or sets the type of the field.
        //
        // Returns:
        //     A Microsoft.SharePoint.SPFieldType value that specifies the type of field.
        public SPFieldType Type { get; set; }
        //
        // Summary:
        //     Gets the type of the field as a string value.
        //
        // Returns:
        //     A string that represents the field type.
        public string TypeAsString { get; set; }
        public virtual string TypeDisplayName { get; set; }
        public string TypeShortDescription { get; set; }
        public bool UsedInWebContentTypes { get; set; }
        public int Version { get; set; }
        public string XPath { get; set; }


    }

    public static class SPFieldEx
    {
        /// <summary>
        /// Get StaticName and InternalName from Title through DecodeFriendlyAndRemoveNonAlphaNumericChars, on SPBuiltInField leaves original names
        /// </summary>
        /// <param name="field"></param>
        /// <param name="staticName"></param>
        /// <param name="internalName"></param>
        public static void GetStaticNameAndInternalNameFromTitle(this SPField field, out string staticName, out string internalName)
        {

            if (!SPBuiltInFieldId.Contains(field.Id))
            {
                staticName = SPStringUtilityEx.DecodeFriendlyAndRemoveNonAlphaNumericChars(field.Title);
                internalName = staticName;
                if (internalName.Length > 32)
                {
                    internalName = staticName.Substring(0, 31);
                }
            }
            else
            {
                staticName = field.StaticName;
                internalName = field.InternalName;
            }
        }


    }
}
