﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using Microsoft.SharePoint;
using SharePoint.Extensions;
using SharePoint.Extensions.SystemEx;

namespace SharePoint.Extensions
{
    public static class SPContentTypeEx
    {
        static readonly Type spcontenttype = Reflection.SPAssembly.GetType("Microsoft.SharePoint.SPContentType");

        /// <summary>
        /// Get SPContentType
        /// </summary>
        /// <param name="id">string</param>
        /// <returns></returns>
        public static SPContentType GetContentType(this SPContentTypeCollection contenttypecollection, string id)
        {
            if (contenttypecollection == null)
                throw new ArgumentNullException("contenttypecollection");
            if (id == null)
                throw new ArgumentNullException("id");

            SPContentType contenttype = null;
            SPContentTypeId ctid = new SPContentTypeId(id);
            for (int i = 0; i < contenttypecollection.Count; i++)
            {
                if (contenttypecollection[i].Id == ctid)
                {
                    contenttype = contenttypecollection[i];
                    break;
                }
            }
            return contenttype;
        } // end GetContentType

        /// <summary>
        /// Get SPContentType
        /// </summary>
        /// <param name="id">string</param>
        /// <returns></returns>
        public static SPContentType Exists(this SPContentTypeCollection contenttypecollection, SPContentType contentType)
        {
            if (contenttypecollection == null)
                throw new ArgumentNullException("contenttypecollection");
            if (contentType == null)
                throw new ArgumentNullException("contentType");

            SPContentType ct = null;
            for (int i = 0; i < contenttypecollection.Count; i++)
            {
                if (contenttypecollection[i].Id == contentType.Id
                    || contenttypecollection[i].Name.Equals(contentType.Name, StringComparison.InvariantCultureIgnoreCase))
                {
                    ct = contenttypecollection[i];
                    break;
                }
            }
            return ct;
        } // end Exists

        /// <summary>
        /// Get SPContentType
        /// </summary>
        /// <param name="id">string</param>
        /// <returns></returns>
        public static SPContentType Exists(this SPContentTypeCollection contenttypecollection, string id, string name)
        {
            if (contenttypecollection == null)
                throw new ArgumentNullException("contenttypecollection");
            if (id.IsNullOrEmpty() && name.IsNullOrEmpty())
                throw new ArgumentNullException("id, name");

            SPContentType ct = null;
            SPContentTypeId? ctid = null;
            if (id.IsNotNullOrEmpty())
            {
                ctid = new SPContentTypeId(id);
            }
            for (int i = 0; i < contenttypecollection.Count; i++)
            {
                if ((ctid.IsNotNull() && contenttypecollection[i].Id == ctid)
                    || contenttypecollection[i].Name.Equals(name, StringComparison.InvariantCultureIgnoreCase))
                {
                    ct = contenttypecollection[i];
                    break;
                }
            }
            return ct;
        } // end Exists

        public static SPContentType CreateChildWithId(this SPContentType parentContentType, SPContentTypeCollection collection, string name, SPContentTypeId id)
        {
            SPContentType newCT = new SPContentType(parentContentType, collection, name);

            if (id != SPContentTypeId.Empty)
            {
                if (collection.GetContentType(id.ToString()).IsNotNull())
                {
                    throw new SPException("CreateChildWithId: Content Type Id {0} exists".DoFormat(id.ToString()));
                }
                FieldInfo field = spcontenttype.GetField("m_id", BindingFlags.NonPublic | BindingFlags.Instance);
                field.SetValue(newCT, id);
            }

            return newCT;
        } // end CreateChildWithId

        public static bool IsSameAsParent(this SPContentType ct)
        {
            bool issame = false;
            if (ct.Parent.IsNotNull())
            {
                issame = (ct.Version == ct.Parent.Version);
            }

            return issame;
        } // end IsSameAsParent

        public static bool IsOlderThanParent(this SPContentType ct)
        {
            bool isolder = false;
            if (ct.Parent.IsNotNull())
            {
                isolder = (ct.Version < ct.Parent.Version);
            }
            return isolder;
        } // end IsOlderThanParent

        public static bool IsNotNewerThanParent(this SPContentType ct)
        {
            bool isnewer = false;
            if (ct.Parent.IsNotNull())
            {
                isnewer = (ct.Version <= ct.Parent.Version);
            }
            return isnewer;
        } // end IsNotNewerThanParent

        public static bool IsBuiltIn(this SPContentType ct)
        {
            return SPBuiltInContentTypeId.Contains(ct.Id);
        } // end IsBuiltIn

        public static string SchemaXmlFieldLinks(this SPContentType ct)
        {
            StringBuilder sb = new StringBuilder();
            XmlTextWriter xwtr = new XmlTextWriter(new StringWriter(sb, CultureInfo.InvariantCulture));
            spcontenttype.InvokeMember("SaveCore", BindingFlags.InvokeMethod | BindingFlags.Instance | BindingFlags.NonPublic, null, ct, new object[] { xwtr, false, false });
            xwtr.Flush();
            xwtr.Close();
            return sb.ToString();
        } // end SchemaXmlFieldLinks
    } // end class SPContentTypeEx
} // end namespace SharePoint.Extensions
