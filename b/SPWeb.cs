﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint;
using SharePoint.Extensions.SystemEx;

namespace SharePoint.Extensions
{
    public static class SPWebEx
    {

        public static SPFile GetFileIfExists(this SPWeb web, string fileRelativePath)
        {
            SPFile file = null;
            if (web.GetFile(fileRelativePath).Exists)
            {
                file = web.GetFile(fileRelativePath);
            }
            return file;
        }

        /// <summary>
        /// Get RootWeb of SPWeb
        /// </summary>
        /// <returns>SPWeb</returns>
        public static SPWeb GetRootWeb(this SPWeb web)
        {
            if (web.IsNull())
                throw new ArgumentNullException("Parameter web cannot be null.");

            SPWeb rootweb = web.IsRootWeb ? web : web.Site.RootWeb;
            return rootweb;
        } // end GetRootWeb

        /// <summary>
        /// Get SPList
        /// </summary>
        /// <param name="web">SPWeb</param>
        /// <param name="guid">Guid</param>
        /// <returns>SPList or null if list not exists</returns>
        public static SPList GetList(this SPWeb web, Guid guid)
        {
            if (web.IsNull())
                throw new ArgumentNullException("Parameter web cannot be null.");
            if (guid.IsNull())
                throw new ArgumentNullException("Parameter giid cannot be null.");

            SPList list = null;
            SPListCollection listcollection = web.Lists;
            for (int i = 0; i < listcollection.Count; i++)
            {
                if (listcollection[i].ID == guid)
                {
                    list = listcollection[i];
                    break;
                }
            }
            return list;
        } // end GetList

        /// <summary>
        /// Get SPList
        /// </summary>
        /// <param name="web">SPWeb</param>
        /// <param name="name">string</param>
        /// <returns>SPList or null if list not exists</returns>
        public static SPList GetList(this SPWeb web, string name)
        {
            if (web.IsNull())
                throw new ArgumentNullException("Parameter web cannot be null.");
            if (name.IsNullOrEmpty())
                throw new ArgumentNullException("Parameter name cannot be null.");

            SPList list = null;
            SPListCollection listcollection = web.Lists;
            for (int i = 0; i < listcollection.Count; i++)
            {
                if (listcollection[i].Title == name)
                {
                    list = listcollection[i];
                    break;
                }
            }
            return list;
        } // end GetList

        public static SPList GetListByUrl(this SPWeb web, string relativeSPWebUrl)
        {
            return web.GetListByUrl(relativeSPWebUrl, true);
        } // end GetListByUrl

        public static SPList GetListByUrl(this SPWeb web, string relativeSPWebUrl, bool throwerror)
        {
            SPList list = null;
            try
            {
                list = web.GetList(new Uri(web.Url).AbsolutePath.AppendToURL(relativeSPWebUrl));
            }
            catch
            {
                if (throwerror)
                    throw;
            }
            return list;
        } // end GetListByUrl

        //TODO: relativeurl
        //getlistbyfullurl
        //string lurl = (web.ServerRelativeUrl == "/" ? string.Empty : web.ServerRelativeUrl);
        ///replace is case sensitive!!!
        //lurl += url.Replace(web.Url, "");
    } // end class SPWebEx
} // end namespace SharePoint.Extensions
